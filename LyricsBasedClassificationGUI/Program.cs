﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LyricsBasedClassificationGUI
{
    static class Program
    {
        public static PrivateFontCollection HindiFontCollection()
        {
            PrivateFontCollection pfc = new PrivateFontCollection();
            String path = "~/../../../fonts/";
            String ext=".ttf";
            String APARAJ = path + "APARAJ" + ext;
            String DEVANAGARI_NEW = path + "DEVANAGARI_NEW" + ext;
            String KOKILA = path + "KOKILA" + ext;
            String MANGAL = path + "MANGAL" + ext;
            pfc.AddFontFile(APARAJ);
            pfc.AddFontFile(DEVANAGARI_NEW);
            pfc.AddFontFile(KOKILA);
            pfc.AddFontFile(MANGAL);
            return pfc;
        }
        public static Font GetHindiFont(LyricsBasedClassificationGUI.Basic.HindiFontTypes fontType,float size)
        {
            PrivateFontCollection pfc = HindiFontCollection();
            switch (fontType)
            {
                case LyricsBasedClassificationGUI.Basic.HindiFontTypes.APARAJ:
                    return new Font(pfc.Families[0], size, FontStyle.Regular);
                case LyricsBasedClassificationGUI.Basic.HindiFontTypes.APARAJ_BOLD:
                    return new Font(pfc.Families[0], size, FontStyle.Bold);
                case LyricsBasedClassificationGUI.Basic.HindiFontTypes.APARAJ_BOLD_ITALIC:
                    return new Font(pfc.Families[0], size, FontStyle.Bold | FontStyle.Italic);
                case LyricsBasedClassificationGUI.Basic.HindiFontTypes.APARAJ_ITALIC:
                    return new Font(pfc.Families[0], size, FontStyle.Italic);
                case LyricsBasedClassificationGUI.Basic.HindiFontTypes.DEVANAGARI_NEW:
                    return new Font(pfc.Families[1], size, FontStyle.Regular);
                case LyricsBasedClassificationGUI.Basic.HindiFontTypes.KOKILA:
                    return new Font(pfc.Families[2], size, FontStyle.Regular);
                case LyricsBasedClassificationGUI.Basic.HindiFontTypes.KOKILA_BOLD:
                    return new Font(pfc.Families[2], size, FontStyle.Bold);
                case LyricsBasedClassificationGUI.Basic.HindiFontTypes.KOKILA_BOLD_ITALIC:
                    return new Font(pfc.Families[2], size, FontStyle.Bold | FontStyle.Italic);
                case LyricsBasedClassificationGUI.Basic.HindiFontTypes.KOKILA_ITALIC:
                    return new Font(pfc.Families[2], size, FontStyle.Italic);
                case LyricsBasedClassificationGUI.Basic.HindiFontTypes.MANGAL:
                    return new Font(pfc.Families[3], size, FontStyle.Regular);
                case LyricsBasedClassificationGUI.Basic.HindiFontTypes.MANGAL_BOLD:
                    return new Font(pfc.Families[3], size, FontStyle.Bold);
                default:
                    return new Font(pfc.Families[3], size,FontStyle.Regular);
            }
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new StartApp());
        }
    }
}
