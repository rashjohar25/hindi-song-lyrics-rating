﻿namespace LyricsBasedClassificationGUI
{
    partial class StartApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartApp));
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.selectFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DataProcessorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tokenizerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FileSelector = new System.Windows.Forms.OpenFileDialog();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.button_tokenizer = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.common_ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.taskRunner = new System.ComponentModel.BackgroundWorker();
            this.menuMain.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectFileToolStripMenuItem,
            this.DataProcessorToolStripMenuItem});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(651, 24);
            this.menuMain.TabIndex = 0;
            this.menuMain.Text = "menuStrip1";
            // 
            // selectFileToolStripMenuItem
            // 
            this.selectFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFleToolStripMenuItem});
            this.selectFileToolStripMenuItem.Name = "selectFileToolStripMenuItem";
            this.selectFileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.selectFileToolStripMenuItem.Text = "File";
            // 
            // openFleToolStripMenuItem
            // 
            this.openFleToolStripMenuItem.Name = "openFleToolStripMenuItem";
            this.openFleToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openFleToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.openFleToolStripMenuItem.Text = "Open Fle";
            this.openFleToolStripMenuItem.Click += new System.EventHandler(this.openFleToolStripMenuItem_Click);
            // 
            // DataProcessorToolStripMenuItem
            // 
            this.DataProcessorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tokenizerToolStripMenuItem});
            this.DataProcessorToolStripMenuItem.Name = "DataProcessorToolStripMenuItem";
            this.DataProcessorToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.DataProcessorToolStripMenuItem.Text = "Data Processor";
            // 
            // tokenizerToolStripMenuItem
            // 
            this.tokenizerToolStripMenuItem.Name = "tokenizerToolStripMenuItem";
            this.tokenizerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.tokenizerToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.tokenizerToolStripMenuItem.Text = "Tokenizer";
            this.tokenizerToolStripMenuItem.Click += new System.EventHandler(this.tokenizerToolStripMenuItem_Click);
            // 
            // FileSelector
            // 
            this.FileSelector.Filter = "Text File|*.txt|2003 Word File|*.doc|2007 Word File|*.docx|PDF Document|*.pdf";
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Font = new System.Drawing.Font("Mangal", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(0, 49);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(651, 337);
            this.textBox1.TabIndex = 1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.button_tokenizer});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(651, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // button_tokenizer
            // 
            this.button_tokenizer.Image = ((System.Drawing.Image)(resources.GetObject("button_tokenizer.Image")));
            this.button_tokenizer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.button_tokenizer.Name = "button_tokenizer";
            this.button_tokenizer.Size = new System.Drawing.Size(78, 22);
            this.button_tokenizer.Text = "Tokenizer";
            this.button_tokenizer.Click += new System.EventHandler(this.button_tokenizer_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.common_ProgressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 364);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(651, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // common_ProgressBar
            // 
            this.common_ProgressBar.AutoSize = false;
            this.common_ProgressBar.Name = "common_ProgressBar";
            this.common_ProgressBar.Size = new System.Drawing.Size(200, 16);
            this.common_ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.common_ProgressBar.Visible = false;
            // 
            // taskRunner
            // 
            this.taskRunner.DoWork += new System.ComponentModel.DoWorkEventHandler(this.taskRunner_DoWork);
            this.taskRunner.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.taskRunner_ProgressChanged);
            this.taskRunner.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.taskRunner_RunWorkerCompleted);
            // 
            // StartApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 386);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuMain);
            this.MainMenuStrip = this.menuMain;
            this.Name = "StartApp";
            this.Text = "Form1";
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem selectFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFleToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog FileSelector;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripButton button_tokenizer;
        private System.Windows.Forms.ToolStripProgressBar common_ProgressBar;
        private System.ComponentModel.BackgroundWorker taskRunner;
        private System.Windows.Forms.ToolStripMenuItem DataProcessorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tokenizerToolStripMenuItem;

    }
}

