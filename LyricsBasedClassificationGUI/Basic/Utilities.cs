﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LyricsBasedClassificationGUI.Basic
{
    public class Utilities
    {
        public static readonly String STATUS_READ_FILE="READ_FILE";
        public static void LoadForm<T>() where T:Form,new()
        {
            T form;
            if (Application.OpenForms.OfType<T>().Count() > 0)
            {
                form = Application.OpenForms.OfType<T>().ElementAt(0);
                form.Show();
            }
            else
            {
                form = new T();
                form.Show();
            }
        }
    }
}
