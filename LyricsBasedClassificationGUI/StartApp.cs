﻿using HindiSongLyricsClassification.FileReading;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;

namespace LyricsBasedClassificationGUI
{
    public partial class StartApp : Form
    {
        public StartApp()
        {
            InitializeComponent();
            textBox1.Font = Program.GetHindiFont(Basic.HindiFontTypes.MANGAL,12);
        }
        static String fileData;

        private void openFleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FileSelector.ShowDialog().Equals(DialogResult.OK))
            {
                fileData = TextFileReader.ReadSongFile(new FileInfo(FileSelector.FileName));
                if (!String.IsNullOrEmpty(fileData))
                {
                    textBox1.Text = fileData;
                }
                else
                {
                    MessageBox.Show("Not a valid File to Read the Song Data.", "Invalid File or Data.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button_tokenizer_Click(object sender, EventArgs e)
        {
            Font font = textBox1.Font;
            byte[] textdata32=System.Text.Encoding.UTF32.GetBytes(textBox1.Text);


            List<String> tokens=DataProcessor.Tokenizer(textBox1.Text);
            String str = "";
            foreach (String item in tokens)
            {
                str += item + HindiSongLyricsClassification.BaseClasses.Constants.INSERT_NEWLINE;
            }
            str=str.Remove(str.LastIndexOf(HindiSongLyricsClassification.BaseClasses.Constants.INSERT_NEWLINE));
            textBox1.Text = str;
        }
        private void taskRunner_DoWork(object sender, DoWorkEventArgs e)
        {
            if(e.Argument.ToString().Equals(Basic.Utilities.STATUS_READ_FILE))
            {
                
            }
        }

        private void taskRunner_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void taskRunner_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
        }
        private void tokenizerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button_tokenizer.PerformClick();
        }
    }
}
