﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HindiSongLyricsClassification.FileReading
{
    public class TextFileReader
    {
        private static String ReadDoc(FileInfo file)
        {
            StringBuilder text = new StringBuilder();
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
            object miss = System.Reflection.Missing.Value;
            object path = @file.FullName;
            object readOnly = true;
            Microsoft.Office.Interop.Word.Document docs = word.Documents.Open(ref path, ref miss, ref readOnly, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);
            for (int i = 0; i < docs.Paragraphs.Count; i++)
            {
                text.Append(BaseClasses.Constants.INSERT_NEWLINE + docs.Paragraphs[i + 1].Range.Text.ToString());
            }
            String data = text.ToString().Replace("\v", BaseClasses.Constants.INSERT_NEWLINE);
            return data;
        }
        private static String ReadPDF(FileInfo file)
        {
            StringBuilder text = new StringBuilder();
            using (PdfReader reader = new PdfReader(file.FullName))
            {
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                }
            }
            return text.ToString();
        }
        private static String ReadText(FileInfo file)
        {
            string text = System.IO.File.ReadAllText(@file.FullName);
            return text.ToString();
        }
        public static String ReadSongFile(FileInfo file)
        {
            if (file.Extension.Equals(".doc") || file.Extension.Equals(".docx"))
            {
                return ReadDoc(file);
            }
            else if (file.Extension.Equals(".pdf"))
            {
                return ReadPDF(file);
            }
            else if (file.Extension.Equals(".txt"))
            {
                return ReadText(file);
            }
            else
            {
                return String.Empty;
            }
        }
    }
}
