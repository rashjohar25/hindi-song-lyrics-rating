﻿using HindiSongLyricsClassification.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HindiSongLyricsClassification.FileReading
{
    public class DataProcessor
    {
        public static List<String> Tokenizer(String textdata)
        {
            List<String> data = new List<string>();
            foreach (String item in textdata.Split(Constants.SPACE_SPLIT, StringSplitOptions.RemoveEmptyEntries))
	        {
		        if(String.IsNullOrEmpty(item)||String.IsNullOrWhiteSpace(item))
                {

                }
                else
                {
                    data.Add(item);
                }
	        }
            foreach (String item in data)
            {
                item.Replace(Constants.SPACE_STRING, ""); 
                item.Trim();
            }
            return data;
        }
    }
}
