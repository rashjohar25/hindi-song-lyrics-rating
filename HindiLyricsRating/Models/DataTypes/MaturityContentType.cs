﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HindiLyricsRating.Models.DataTypes
{
    public enum MaturityContentType
    {
        Everyone = 2,//For Child under the age of 4
        Very_Low_Maturity = 4,//For Child between the age of 4 and 9
        Low_Maturity = 9,//For middle childhood age between the age of 9 and 12
        Medium_Maturity = 12,//For Teenager between the age of 12 and 17
        High_Maturity = 17,//For people age above 17
    }
}
