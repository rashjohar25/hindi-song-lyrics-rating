﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HindiLyricsRating.Models.DataTypes
{
    public enum HindiFontTypes
    {
        APARAJ, 
        APARAJ_BOLD,
        APARAJ_BOLD_ITALIC,
        APARAJ_ITALIC,
        DEVANAGARI_NEW,
        KOKILA,
        KOKILA_BOLD,
        KOKILA_BOLD_ITALIC,
        KOKILA_ITALIC,
        MANGAL,
        MANGAL_BOLD
    }
}
