﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HindiLyricsRating.Models.FileHandling
{
    public class TextFileReader
    {
        private static String RemoveEmptyChars(String filedata)
        {
            String data = filedata.Replace("\v", Models.Basic.Utilities.INSERT_NEWLINE);
            
            return data;
        }
        private static String ReadDocAsync(FileInfo file)
        {
            StringBuilder text = new StringBuilder();
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
            object miss = System.Reflection.Missing.Value;
            object path = @file.FullName;
            object readOnly = true;
            Microsoft.Office.Interop.Word.Document docs = word.Documents.Open(ref path, ref miss, ref readOnly, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);
            for (int i = 0; i < docs.Paragraphs.Count; i++)
            {
                text.Append(Models.Basic.Utilities.INSERT_NEWLINE + docs.Paragraphs[i + 1].Range.Text.ToString());
            }
            ((_Document)docs).Close();
            ((_Application)word).Quit();
            return RemoveEmptyChars(text.ToString());
        }
        private static String ReadPDFAsync(FileInfo file)
        {
            StringBuilder text = new StringBuilder();
            using (PdfReader reader = new PdfReader(file.FullName))
            {
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                }
            }
            return RemoveEmptyChars(text.ToString());
        }
        private static String ReadTextAsync(FileInfo file)
        {
            string text = System.IO.File.ReadAllText(@file.FullName);
            return RemoveEmptyChars(text.ToString());
        }
        public static String ReadSongFile(FileInfo file)
        {
            if (file.Extension.Equals(".doc") || file.Extension.Equals(".docx"))
            {
                return ReadDocAsync(file);
            }
            else if (file.Extension.Equals(".pdf"))
            {
                return ReadPDFAsync(file);
            }
            else if (file.Extension.Equals(".txt"))
            {
                return ReadTextAsync(file);
            }
            else
            {
                return String.Empty;
            }
        }
    }
}
