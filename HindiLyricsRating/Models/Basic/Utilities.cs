﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HindiLyricsRating.Models.Basic
{
    public class Utilities
    {
        public static char CharFromValue(int value)
        {
            return (char)value;
        }
        public static char[] CHAR_TOKENIZERS
        {
            get
            {
                List<char> list = new List<char>();
                list.Add(CharFromValue(160));
                list.Add(' ');
                list.Add('\n');
                list.Add(',');
                return list.ToArray<char>();
            }
        }
        public static readonly String INSERT_NEWLINE = "\r\n";
        public static readonly String SPACE_STRING = " ";
        public static readonly String STATUS_READ_FILE="READ_FILE";
        public static readonly char[] ESCAPE_CHARACTERS = { ',' };
        public static void LoadForm<T>() where T:Form,new()
        {
            T form;
            if (Application.OpenForms.OfType<T>().Count() > 0)
            {
                form = Application.OpenForms.OfType<T>().ElementAt(0);
                form.Show();
            }
            else
            {
                form = new T();
                form.Show();
            }
        }
    }
}
