﻿using HindiLyricsRating.Models.Basic;
using HindiLyricsRating.Models.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HindiLyricsRating.Models.TextOperations
{
    public class NgramMaker
    {
        public static IEnumerable<String> MakeGrams(String[] text, int size)
        {
            return null;
        }
        public static IEnumerable<SingleIndexedWord> MakeGramsFromIndexedWords(List<SingleIndexedWord> data, int size)
        {
            List<SingleIndexedWord> list = new List<SingleIndexedWord>();
            if(data!=null)
            {
                if(data.Count>0)
                {
                    String result;
                    for (int i = 0; i < data.Count-(size-1); i++)
                    {
                        result = "";
                        for (int j = i; j <=i+(size-1); j++)
                        {
                            if(String.IsNullOrEmpty(result))
                            {
                                result += data.ElementAt(j).Word;
                            }
                            else
                            {
                                result += " " + data.ElementAt(j).Word;
                            }
                        }
                        list.Add(new SingleIndexedWord(i, result));
                    }
                    return list;
                }
                return list;
            }
            return list;
        }
        public static IEnumerable<String> makeNgrams(String text,int size)
        {
            
            StringBuilder nGram = new StringBuilder();
            Queue<int> wordLengths = new Queue<int>();
            String[] listofAllWords = text.Split(Utilities.CHAR_TOKENIZERS, StringSplitOptions.RemoveEmptyEntries);
            int wordCount = 0;
            int lastWordLen = 0;

            if(text!=""&&char.IsLetterOrDigit(text[0]))
            {
                nGram.Append(text[0]);
                lastWordLen++;
            }
            for (int i = 1; i < text.Length-1; i++)
            {
                char before = text[i - 1];
                char after = text[i + 1];
                if(char.IsLetterOrDigit(text[i])||(text[i]!=' '&&(char.IsSeparator(text[i])||char.IsPunctuation(text[i]))&&(char.IsLetterOrDigit(before)&&char.IsLetterOrDigit(after))))
                {
                    nGram.Append(text[i]);
                    lastWordLen++;
                }
                else
                {
                    if(lastWordLen>0)
                    {
                        wordLengths.Enqueue(lastWordLen);
                        lastWordLen = 0;
                        wordCount++;
                        if(wordCount>=size)
                        {
                            yield return nGram.ToString();
                            nGram.Remove(0, wordLengths.Dequeue() + 1);
                            wordCount -= 1;
                        }
                        nGram.Append(" ");
                    }
                }
            }
        }
    }
}
