﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HindiLyricsRating.Models.TextOperations
{
    public class RemoveEmptyCharacters
    {
        public static String RemoveEmptyByCharNumbers(String text,int number)
        {
            text=text.Trim();
            char c = (char)160;
            char[] p = new char[1];
            p[0] = c;
            text=text.Replace(c.ToString(), Basic.Utilities.INSERT_NEWLINE);
            text = text.Replace("\r" + Basic.Utilities.INSERT_NEWLINE, Basic.Utilities.INSERT_NEWLINE);
            text = text.Replace(Basic.Utilities.INSERT_NEWLINE + Basic.Utilities.INSERT_NEWLINE, Basic.Utilities.INSERT_NEWLINE);
            return text;
        }
    }
}
