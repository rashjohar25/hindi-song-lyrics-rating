﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using HindiLyricsRating.Models.Objects;

namespace HindiLyricsRating.Models.TextOperations
{
    public class HindiStemmerLight
    {
        static private Dictionary<String, String> cache = new Dictionary<String, String>();
        private StringBuilder sb = new StringBuilder();
        public HindiStemmerLight()
        {
        }
        public String stem(String word)
        {
            String data;
            if(cache.ContainsKey(word))
            {
                data = cache[word];
                if (data != null) return data;
                sb.Remove(0, sb.Length);
                sb.Append(word);
                data=remove_suffix(sb.ToString());
                cache.Add(word, data);
                return data;
            }
            else
            {
                sb.Remove(0, sb.Length);
                sb.Append(word);
                data=remove_suffix(sb.ToString());
                cache.Add(word, data);
                return data;
            }
        }
        private String remove_suffix(String word)
        {
            List<SingleIndexedWord> suffixes = Program.StemmingWordHindi();
            List<byte[]> suff = new List<byte[]>();
            foreach (SingleIndexedWord item in suffixes)
            {
                suff.Add(System.Text.Encoding.Unicode.GetBytes(item.Word));
            }
            int len = word.Length - 1;
            int neg1 = len - 1;
            int neg2 = len - 2;
            int pos1 = len+1;
            if (len > 4)
            {
                if(word.EndsWith(suffixes.ElementAt(0).Word))
                {
                    return word.Remove(neg2);
                }
                if (word.EndsWith(suffixes.ElementAt(1).Word))
                {
                    return word.Remove(neg2);
                }
            }
            if (len > 3)
            {
                if (word.EndsWith(suffixes.ElementAt(2).Word))
                {
                    return word.Remove(neg1);
                }
                if (word.EndsWith(suffixes.ElementAt(3).Word))
                {
                    return word.Remove(neg1);
                }
                if (word.EndsWith(suffixes.ElementAt(4).Word))
                {
                    return word.Remove(neg1);
                }
                if (word.EndsWith(suffixes.ElementAt(5).Word))
                {
                    return word.Remove(neg1);
                }
                if (word.EndsWith(suffixes.ElementAt(6).Word))
                {
                    return word.Remove(neg1);
                }
                if (word.EndsWith(suffixes.ElementAt(7).Word))
                {
                    return word.Remove(neg1);
                }
                if (word.EndsWith(suffixes.ElementAt(8).Word))
                {
                    return word.Remove(neg1);
                }
                if (word.EndsWith(suffixes.ElementAt(9).Word))
                {
                    return word.Remove(neg1);
                }
                if (word.EndsWith(suffixes.ElementAt(10).Word))
                {
                    return word.Remove(neg1);
                }
                if (word.EndsWith(suffixes.ElementAt(11).Word))
                {
                    return word.Remove(neg1);
                }
                if (word.EndsWith(suffixes.ElementAt(12).Word))
                {
                    return word.Remove(neg1);
                }
            }
            if (len > 2)
            {
                if (word.EndsWith(suffixes.ElementAt(13).Word))
                {
                    return word.Remove(len);
                }
                if (word.EndsWith(suffixes.ElementAt(14).Word))
                {
                    return word.Remove(len);
                }
                if (word.EndsWith(suffixes.ElementAt(15).Word))
                {
                    return word.Remove(len);
                }
                if (word.EndsWith(suffixes.ElementAt(16).Word))
                {
                    return word.Remove(len);
                }
                if (word.EndsWith(suffixes.ElementAt(17).Word))
                {
                    return word.Remove(len);
                }
                if (word.EndsWith(suffixes.ElementAt(18).Word))
                {
                    return word.Remove(len);
                }
            }
            return word;
        }
    }
}
