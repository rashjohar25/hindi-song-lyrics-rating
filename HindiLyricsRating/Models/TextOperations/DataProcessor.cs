﻿using HindiLyricsRating.Models.Basic;
using HindiLyricsRating.Models.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HindiLyricsRating.Models.TextOperations
{
    public class DataProcessor
    {
        public static List<String> Tokenizer(String textdata)
        {
            List<String> data1 = new List<string>();
            foreach (String item in textdata.Split(Utilities.CHAR_TOKENIZERS,StringSplitOptions.RemoveEmptyEntries))
            {
                if (!(String.IsNullOrEmpty(item) || String.IsNullOrWhiteSpace(item)))
                {
                    String itp = item.Trim().Replace(Utilities.SPACE_STRING, "").Replace(((char)160).ToString(), Basic.Utilities.INSERT_NEWLINE)
                        .Replace("\r" + Basic.Utilities.INSERT_NEWLINE, Basic.Utilities.INSERT_NEWLINE)
                        .Replace(Basic.Utilities.INSERT_NEWLINE + Basic.Utilities.INSERT_NEWLINE, Basic.Utilities.INSERT_NEWLINE);
                    data1.Add(itp);
                }
            }


            //List<String> data = new List<string>();
            //foreach (String item in textdata.Split(Utilities.SPACE_STRING.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            //{
            //    if (String.IsNullOrEmpty(item) || String.IsNullOrWhiteSpace(item))
            //    {

            //    }
            //    else
            //    {
            //        data.Add(item);
            //    }
            //}
            //foreach (String item in data)
            //{
            //    item.Replace(Utilities.SPACE_STRING, "");
            //    item.Trim();
            //}
            //foreach (String item in data)
            //{
            //    str += item + Utilities.INSERT_NEWLINE;
            //}
            //str = RemoveEmptyCharacters.RemoveEmptyByCharNumbers(str, 160);
            //data.Clear();
            //String[] aray = new String[1];
            //aray[0] = Utilities.INSERT_NEWLINE;
            //foreach (String item in str.Split(aray, StringSplitOptions.RemoveEmptyEntries))
            //{
            //    if (String.IsNullOrEmpty(item) || String.IsNullOrWhiteSpace(item))
            //    {

            //    }
            //    else
            //    {
            //        data.Add(item);
            //    }
            //}
            
            return data1;
        }
    }
}
