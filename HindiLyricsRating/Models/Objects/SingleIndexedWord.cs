﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HindiLyricsRating.Models.Objects
{
    public class SingleIndexedWord
    {
        public int Index { get; set; }
        public String Word { get; set; }
        public SingleIndexedWord(int index,String word)
        {
            this.Index = index;
            this.Word = word;
        }
        public Boolean Equals(SingleIndexedWord word)
        {
            char[] myword = this.Word.ToCharArray();
            char[] otherword = word.Word.ToCharArray();
            if(myword.Length==otherword.Length)
            {
                for (int i = 0; i < myword.Length; i++)
                {
                    if(myword[i]!=otherword[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean PresentinList(List<SingleIndexedWord> list)
        {
            foreach (SingleIndexedWord item in list)
            {
                if(this.Equals(item))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
