﻿using HindiLyricsRating.Models.Basic;
using HindiLyricsRating.Models.FileHandling;
using HindiLyricsRating.Models.Objects;
using HindiLyricsRating.Models.TextOperations;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HindiLyricsRating
{
    static class Program
    {
        private readonly static String RELATIVEPATH="~/../../../";
        public static List<SingleIndexedWord> StemmingWordHindi()
        {
            List<SingleIndexedWord> allWords = new List<SingleIndexedWord>();
            String FilePath = RELATIVEPATH + "files/StemWords/CHindi.txt";
            String fileData = TextFileReader.ReadSongFile(new FileInfo(FilePath));
            String[] aray = new String[1];
            aray[0] = Utilities.INSERT_NEWLINE;
            List<String> data = new List<string>();
            foreach (String item in fileData.Split(aray, StringSplitOptions.RemoveEmptyEntries))
            {
                if (String.IsNullOrEmpty(item) || String.IsNullOrWhiteSpace(item))
                {

                }
                else
                {
                    data.Add(item);
                }
            }
            for (int i = 0; i < data.Count; i++)
            {
                allWords.Add(new SingleIndexedWord(i, data.ElementAt(i).Replace("s(\"", "")));
            }
            return allWords;
        }
        public static List<SingleIndexedWord> StoppingWordEnglish()
        {
            List<SingleIndexedWord> allWords = new List<SingleIndexedWord>();
            String FilePath = RELATIVEPATH + "files/StopWords/English.txt";
            String fileData = TextFileReader.ReadSongFile(new FileInfo(FilePath));
            List<String> tokens = DataProcessor.Tokenizer(fileData);
            for (int i = 0; i < tokens.Count; i++)
            {
                allWords.Add(new SingleIndexedWord(i, tokens.ElementAt(i)));
            }
            return allWords;
        }
        public static List<SingleIndexedWord> StoppingWord()
        {
            List<SingleIndexedWord> allWords = new List<SingleIndexedWord>();
            String FilePath = RELATIVEPATH + "files/StopWords/Hindi.txt";
            String fileData = TextFileReader.ReadSongFile(new FileInfo(FilePath));
            List<String> tokens = DataProcessor.Tokenizer(fileData);
            for (int i = 0; i < tokens.Count; i++)
            {
                allWords.Add(new SingleIndexedWord(i, tokens.ElementAt(i)));
            }
            return allWords;
        }
        public static PrivateFontCollection HindiFontCollection()
        {
            PrivateFontCollection pfc = new PrivateFontCollection();
            String path = RELATIVEPATH+"files/fonts/";
            String ext = ".ttf";
            String APARAJ = path + "APARAJ" + ext;
            String DEVANAGARI_NEW = path + "DEVANAGARI_NEW" + ext;
            String KOKILA = path + "KOKILA" + ext;
            String MANGAL = path + "MANGAL" + ext;
            pfc.AddFontFile(APARAJ);
            pfc.AddFontFile(DEVANAGARI_NEW);
            pfc.AddFontFile(KOKILA);
            pfc.AddFontFile(MANGAL);
            return pfc;
        }
        public static Font GetHindiFont(HindiLyricsRating.Models.DataTypes.HindiFontTypes fontType, float size)
        {
            PrivateFontCollection pfc = HindiFontCollection();
            switch (fontType)
            {
                case HindiLyricsRating.Models.DataTypes.HindiFontTypes.APARAJ:
                    return new Font(pfc.Families[0], size, FontStyle.Regular);
                case HindiLyricsRating.Models.DataTypes.HindiFontTypes.APARAJ_BOLD:
                    return new Font(pfc.Families[0], size, FontStyle.Bold);
                case HindiLyricsRating.Models.DataTypes.HindiFontTypes.APARAJ_BOLD_ITALIC:
                    return new Font(pfc.Families[0], size, FontStyle.Bold | FontStyle.Italic);
                case HindiLyricsRating.Models.DataTypes.HindiFontTypes.APARAJ_ITALIC:
                    return new Font(pfc.Families[0], size, FontStyle.Italic);
                case HindiLyricsRating.Models.DataTypes.HindiFontTypes.DEVANAGARI_NEW:
                    return new Font(pfc.Families[1], size, FontStyle.Regular);
                case HindiLyricsRating.Models.DataTypes.HindiFontTypes.KOKILA:
                    return new Font(pfc.Families[2], size, FontStyle.Regular);
                case HindiLyricsRating.Models.DataTypes.HindiFontTypes.KOKILA_BOLD:
                    return new Font(pfc.Families[2], size, FontStyle.Bold);
                case HindiLyricsRating.Models.DataTypes.HindiFontTypes.KOKILA_BOLD_ITALIC:
                    return new Font(pfc.Families[2], size, FontStyle.Bold | FontStyle.Italic);
                case HindiLyricsRating.Models.DataTypes.HindiFontTypes.KOKILA_ITALIC:
                    return new Font(pfc.Families[2], size, FontStyle.Italic);
                case HindiLyricsRating.Models.DataTypes.HindiFontTypes.MANGAL:
                    return new Font(pfc.Families[3], size, FontStyle.Regular);
                case HindiLyricsRating.Models.DataTypes.HindiFontTypes.MANGAL_BOLD:
                    return new Font(pfc.Families[3], size, FontStyle.Bold);
                default:
                    return new Font(pfc.Families[3], size, FontStyle.Regular);
            }
        }
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LaunchScreen());
        }
    }
}
