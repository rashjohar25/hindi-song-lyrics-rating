﻿using HindiLyricsRating.Models.Basic;
using HindiLyricsRating.Models.FileHandling;
using HindiLyricsRating.Models.Objects;
using HindiLyricsRating.Models.TextOperations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HindiLyricsRating
{
    public partial class LaunchScreen : Form
    {
        private static String Data_String;
        private static List<SingleIndexedWord> allWord;
        public LaunchScreen()
        {
            InitializeComponent();
            textBox1.Font = Program.GetHindiFont(Models.DataTypes.HindiFontTypes.MANGAL, 12);
        }
        private void button_tokenizer_Click(object sender, EventArgs e)
        {
            tokenizerToolStripMenuItem.PerformClick();
        }
        private void button_remove_stopwords_Click(object sender, EventArgs e)
        {
            removeStopWordsToolStripMenuItem.PerformClick();
        }
        private void button_Stemming_Click(object sender, EventArgs e)
        {
            stemmingToolStripMenuItem.PerformClick();
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            nGramMakerToolStripMenuItem.PerformClick();
        }
        private void tokenizerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Font font = textBox1.Font;
            List<String> tokens = DataProcessor.Tokenizer(textBox1.Text);
            for (int i = 0; i < tokens.Count;i++ )
            {
                tokens[i]=tokens[i].Replace(Utilities.ESCAPE_CHARACTERS[0].ToString(), "");
            }
            Data_String = "";
            allWord = new List<SingleIndexedWord>();
            for (int i = 0; i < tokens.Count; i++)
            {
                Data_String += tokens.ElementAt(i) + Utilities.INSERT_NEWLINE;
                allWord.Add(new SingleIndexedWord(i, tokens.ElementAt(i)));
            }

            Data_String = Data_String.Trim();
            textBox1.Text = Data_String;
        }
        private void openFleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FileSelector.ShowDialog().Equals(DialogResult.OK))
            {
                String fileData =TextFileReader.ReadSongFile(new FileInfo(FileSelector.FileName)) ;
                if (!String.IsNullOrEmpty(fileData))
                {
                    textBox1.Text = fileData;
                }
                else
                {
                    MessageBox.Show("Not a valid File to Read the Song Data.", "Invalid File or Data.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void removeStopWordsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<SingleIndexedWord> stopWord = Program.StoppingWord();
            allWord.RemoveAll(m => m.PresentinList(stopWord));
            Data_String = "";
            foreach (SingleIndexedWord item in allWord)
            {
                Data_String += item.Word + Utilities.INSERT_NEWLINE;
            }
            Data_String=Data_String.Trim();
            textBox1.Text = Data_String;
        }
         
        private void stemmingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HindiStemmerLight stemmer = new HindiStemmerLight();
            foreach (SingleIndexedWord item in allWord)
            {
                item.Word = stemmer.stem(item.Word);
            }
            Data_String = "";
            foreach (SingleIndexedWord item in allWord)
            {
                Data_String += item.Word + Utilities.INSERT_NEWLINE;
            }
            Data_String = Data_String.Trim();
            textBox1.Text = Data_String;  
        }

        private void nGramMakerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<SingleIndexedWord> _4gramdata = new List<SingleIndexedWord>();
            _4gramdata.AddRange(allWord);
            _4gramdata.AddRange(NgramMaker.MakeGramsFromIndexedWords(allWord, 2).ToList());
            _4gramdata.AddRange(NgramMaker.MakeGramsFromIndexedWords(allWord, 3).ToList());
            Data_String = "";
            foreach (SingleIndexedWord item in _4gramdata)
            {
                Data_String += item.Word + Utilities.INSERT_NEWLINE;
            }
            Data_String = Data_String.Trim();
            textBox1.Text = Data_String;
        }
    }
}
