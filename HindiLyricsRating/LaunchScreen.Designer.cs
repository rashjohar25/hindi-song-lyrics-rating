﻿namespace HindiLyricsRating
{
    partial class LaunchScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LaunchScreen));
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.selectFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DataProcessorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tokenizerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeStopWordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stemmingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FileSelector = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.common_ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.button_tokenizer = new System.Windows.Forms.ToolStripButton();
            this.button_remove_stopwords = new System.Windows.Forms.ToolStripButton();
            this.button_Stemming = new System.Windows.Forms.ToolStripButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.nGramMakerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.menuMain.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectFileToolStripMenuItem,
            this.DataProcessorToolStripMenuItem});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(861, 24);
            this.menuMain.TabIndex = 4;
            this.menuMain.Text = "menuStrip1";
            // 
            // selectFileToolStripMenuItem
            // 
            this.selectFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFleToolStripMenuItem});
            this.selectFileToolStripMenuItem.Name = "selectFileToolStripMenuItem";
            this.selectFileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.selectFileToolStripMenuItem.Text = "File";
            // 
            // openFleToolStripMenuItem
            // 
            this.openFleToolStripMenuItem.Name = "openFleToolStripMenuItem";
            this.openFleToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openFleToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.openFleToolStripMenuItem.Text = "Open Fle";
            this.openFleToolStripMenuItem.Click += new System.EventHandler(this.openFleToolStripMenuItem_Click);
            // 
            // DataProcessorToolStripMenuItem
            // 
            this.DataProcessorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tokenizerToolStripMenuItem,
            this.removeStopWordsToolStripMenuItem,
            this.stemmingToolStripMenuItem,
            this.nGramMakerToolStripMenuItem});
            this.DataProcessorToolStripMenuItem.Name = "DataProcessorToolStripMenuItem";
            this.DataProcessorToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.DataProcessorToolStripMenuItem.Text = "Data Processor";
            // 
            // tokenizerToolStripMenuItem
            // 
            this.tokenizerToolStripMenuItem.Name = "tokenizerToolStripMenuItem";
            this.tokenizerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.tokenizerToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.tokenizerToolStripMenuItem.Text = "Tokenizer";
            this.tokenizerToolStripMenuItem.Click += new System.EventHandler(this.tokenizerToolStripMenuItem_Click);
            // 
            // removeStopWordsToolStripMenuItem
            // 
            this.removeStopWordsToolStripMenuItem.Name = "removeStopWordsToolStripMenuItem";
            this.removeStopWordsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.T)));
            this.removeStopWordsToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.removeStopWordsToolStripMenuItem.Text = "Remove Stop Words";
            this.removeStopWordsToolStripMenuItem.Click += new System.EventHandler(this.removeStopWordsToolStripMenuItem_Click);
            // 
            // stemmingToolStripMenuItem
            // 
            this.stemmingToolStripMenuItem.Name = "stemmingToolStripMenuItem";
            this.stemmingToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.stemmingToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.stemmingToolStripMenuItem.Text = "Stemming";
            this.stemmingToolStripMenuItem.Click += new System.EventHandler(this.stemmingToolStripMenuItem_Click);
            // 
            // FileSelector
            // 
            this.FileSelector.Filter = "Text File|*.txt|2003 Word File|*.doc|2007 Word File|*.docx|PDF Document|*.pdf";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.common_ProgressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 396);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(861, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // common_ProgressBar
            // 
            this.common_ProgressBar.AutoSize = false;
            this.common_ProgressBar.Name = "common_ProgressBar";
            this.common_ProgressBar.Size = new System.Drawing.Size(200, 16);
            this.common_ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.common_ProgressBar.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.button_tokenizer,
            this.button_remove_stopwords,
            this.button_Stemming,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(861, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // button_tokenizer
            // 
            this.button_tokenizer.Image = ((System.Drawing.Image)(resources.GetObject("button_tokenizer.Image")));
            this.button_tokenizer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.button_tokenizer.Name = "button_tokenizer";
            this.button_tokenizer.Size = new System.Drawing.Size(78, 22);
            this.button_tokenizer.Text = "Tokenizer";
            this.button_tokenizer.Click += new System.EventHandler(this.button_tokenizer_Click);
            // 
            // button_remove_stopwords
            // 
            this.button_remove_stopwords.Image = ((System.Drawing.Image)(resources.GetObject("button_remove_stopwords.Image")));
            this.button_remove_stopwords.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.button_remove_stopwords.Name = "button_remove_stopwords";
            this.button_remove_stopwords.Size = new System.Drawing.Size(134, 22);
            this.button_remove_stopwords.Text = "Remove Stop Words";
            this.button_remove_stopwords.Click += new System.EventHandler(this.button_remove_stopwords_Click);
            // 
            // button_Stemming
            // 
            this.button_Stemming.Image = ((System.Drawing.Image)(resources.GetObject("button_Stemming.Image")));
            this.button_Stemming.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.button_Stemming.Name = "button_Stemming";
            this.button_Stemming.Size = new System.Drawing.Size(82, 22);
            this.button_Stemming.Text = "Stemming";
            this.button_Stemming.Click += new System.EventHandler(this.button_Stemming_Click);
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(0, 49);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(861, 347);
            this.textBox1.TabIndex = 7;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // nGramMakerToolStripMenuItem
            // 
            this.nGramMakerToolStripMenuItem.Name = "nGramMakerToolStripMenuItem";
            this.nGramMakerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.nGramMakerToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.nGramMakerToolStripMenuItem.Text = "nGram Maker";
            this.nGramMakerToolStripMenuItem.Click += new System.EventHandler(this.nGramMakerToolStripMenuItem_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(97, 22);
            this.toolStripButton1.Text = "Apply nGram";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // LaunchScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 418);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuMain);
            this.Controls.Add(this.statusStrip1);
            this.Name = "LaunchScreen";
            this.Text = "Hindi Song Lyrics Rating";
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem selectFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DataProcessorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tokenizerToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog FileSelector;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar common_ProgressBar;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton button_tokenizer;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripMenuItem removeStopWordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton button_remove_stopwords;
        private System.Windows.Forms.ToolStripMenuItem stemmingToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton button_Stemming;
        private System.Windows.Forms.ToolStripMenuItem nGramMakerToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}

